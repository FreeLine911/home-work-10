import React from 'react';
import {CircularProgress} from "@material-ui/core";


export default function LoadUI() {

    return (
        <div className="loading">
            <CircularProgress />
            <p>Please wait, your page is loading...</p>
        </div>

    )
}