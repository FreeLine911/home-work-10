import React from 'react';
import {useSelector} from "react-redux";


export default function ShowUsersInfo() {
        const selectedUser = useSelector((state)=> state.usersManager.selectedUser);

        return (
            <>
            <div>
    <table className="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Phone</th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{selectedUser?.id}</td>
            <td>{selectedUser?.name}</td>
            <td>{selectedUser?.email}</td>
            <td>{selectedUser?.username}</td>
            <td>{selectedUser?.phone}</td>

        </tr>
        </tbody>
    </table>
            </div>
            </>
)

}
