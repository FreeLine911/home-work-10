import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {setUsers, fetchUsers, setLoading, setError, setSelectedUser} from "./redux/actions";
import {Button, CircularProgress, Container} from "@material-ui/core";
import SelectUsers from "./components/SelectUsers";
import './App.css';
import { API_USERS_URI } from "./constants.json";
import ShowUsersInfo from "./components/ShowUsersInfo";
import LoadUI from "./components/LoadUI";

function App() {
  const dispatch = useDispatch();
  const loading = useSelector((state)=> state.usersManager.isLoading);
  const error = useSelector((state)=> state.usersManager.responseError);
  const refreshPage = () => {window.location.reload();}


  useEffect(() => {
      dispatch(setLoading(true));
      fetchUsers(API_USERS_URI)
          .then((response) => response.json())
          .then((data)=>{
              dispatch(setLoading(false))
              dispatch(setUsers(data))
          })
          .catch((err)=>{
              dispatch(setError(err.message));
              dispatch(setLoading(false))
          })

    // fetch(API_USERS_URI)
    //     .then((response) => response.json())
    //     .then((data) => dispatch(setUsers(data)));
  }, [dispatch]);

  return (
    <Container>

        {loading ?
            <LoadUI /> :
            <div>
                {error ?
                    <p>{error} <Button style={{color: 'red', border: '1px solid red'}}  onClick={()=> {refreshPage()}}>  Refresh Page </Button></p> :
                    <div>
                        <SelectUsers />
                        <ShowUsersInfo/>
                    </div>
                }
            </div>
        }


    </Container>
  );
}

export default App;
